var webpack = require("webpack");
const path = require("path");
const VueLoaderPlugin = require("vue-loader/lib/plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  entry: {
    IconEtLine: "./src/IconEtLine.vue",
    IconFontAwesome: "./src/IconFontAwesome.vue",
    IconFontIcon: "./src/IconFontIcons.vue",
    IconGlyphicons: "./src/IconGlyphicons.vue",
    IconIcoFont: "./src/IconIcoFont.vue",
  },
  output: {
    path: path.resolve(__dirname + "/dist/"),
    // publicPath: "dist/",
    filename: "[name].js",
    libraryTarget: "umd",
    library: "[name]",
    umdNamedDefine: true,
  },
  mode: "production",
  optimization: {
    minimize: true,
  },
  resolve: {
    extensions: [".js"],
    alias: {
      vue$: "vue/dist/vue.common.js",
    },
  },
  devtool: "#source-map",
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: path.resolve(__dirname, "node_modules"),
        use: "babel-loader",
      },
      {
        test: /\.vue$/,
        use: "vue-loader",
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: "../",
            },
          },
          "css-loader",
        ],
        // use: ["style-loader", "css-loader"],
      },
      {
        test: /\.(woff|woff2|eot|ttf|svg)(\?.*$|$)/,
        use: {
          loader: "file-loader",
          options: {
            name: "fonts/[name].[ext]",
            esModule: false,
          },
        },
      },
    ],
  },
  externals: {
    vue: "Vue",
  },
  plugins: [
    new webpack.optimize.LimitChunkCountPlugin({
      maxChunks: 1,
    }),
    new MiniCssExtractPlugin({
      filename: "css/[name].css",
      chunkFilename: "css/[id].css",
    }),
    new VueLoaderPlugin(),
  ],
};
