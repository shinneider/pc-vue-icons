# Install:

```
    yarn add "https://shinneider@bitbucket.org/shinneider/pc-vue-icons.git#master"
```

# Usage

1.  Import icons you want:

        import IconGl from "pc-vue-icons/dist/IconGlyphicons";
        import IconIF from "pc-vue-icons/dist/IconIcoFont";
        import IconFI from "pc-vue-icons/dist/IconFontIcon";
        import IconFA from "pc-vue-icons/dist/IconFontAwesome";
        import IconEL from "pc-vue-icons/dist/IconEtLine";

1.  Import Css:

        <style> /* Import in main app only */
        @import '~pc-vue-icons/dist/css/IconGlyphicons.css';
        @import '~pc-vue-icons/dist/css/IconIcoFont.css';
        @import '~pc-vue-icons/dist/css/IconFontIcon.css';
        @import '~pc-vue-icons/dist/css/IconFontAwesome.css';
        @import '~pc-vue-icons/dist/css/IconEtLine.css';
        </style>
        /* Or */
        <style scoped> /* Import in each app */
        @import '~pc-vue-icons/dist/css/IconGlyphicons.css';
        @import '~pc-vue-icons/dist/css/IconIcoFont.css';
        @import '~pc-vue-icons/dist/css/IconFontIcon.css';
        @import '~pc-vue-icons/dist/css/IconFontAwesome.css';
        @import '~pc-vue-icons/dist/css/IconEtLine.css';
        </style>

1.  Put previous icons inside your file components:

        export default {
            name: "HowTo",
            components: {
                IconEL,
                IconFA,
                IconFI,
                IconGl,
                IconIF,
            },
        }

1.  Use then in template:

        <template>
            <div>
                <IconEL icon="et-laptop" color="dark" :hover="true" iconSize="lg" containerSize="sm" />
                <IconFA icon="fa-laptop" iconType="solid" color="dark" :hover="true" iconSize="lg" containerSize="sm" />
                <IconFI icon="icon-laptop" color="dark" :hover="true" iconSize="lg" containerSize="sm" />
                <IconGl icon="glyphicon-asterisk" color="dark" :hover="true" iconSize="lg" containerSize="sm" />
                <IconIF icon="icofont-laptop" color="dark" :hover="true" iconSize="lg" containerSize="sm" />
            </div>
        </template>

    - Explanation:
      Obs: Only Icon parameter is mandatory, all others are optional.

            <IconX
                icon="Icon name here (get on site, or inside files in js folder)"
                iconType="Only for Font Awesome, use one ["regular" (fa - default), "solid" (fas), "brands" (fab), "classic" (fa)]"
                iconColor="Use a hex string, override `color`"
                backgroundColor="Use a hex string, override `color`"
                color="Use one ["" (default), "light", "dark", "transparent", "custom"]"
                border="use one ["" (default), "sm", "md", "lg", "xlg"]"
                corner="use one ["" (default), "rounded", "bordered"]"
                iconSize="The Icon size, use one ["xs", "sm" (default), "md", "lg", "xlg"]"
                containerSize="The container of icon size, use one ["xs", "sm" (default), "md", "lg", "xlg"]"
                :hover="Use one [true, false], if true, on mouse hover, change the color"
            />

1.  if you need to use external style:

        <template>
            <IconX color="custom" ... />
        </template>

        ...

        <style scoped>
            >>> i.ico-custom:hover {
                background-color: #4a4b4a !important;
            }
        </style>
